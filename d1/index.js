// CRUD Operations in MongoDB (As discussed)
// To execute the code in Robo3T, press F5 or Control + Enter


// C - CREATE (Or INSERT)

	// INSERT ONE: To insert one document, we have used the method "insertOne"
	db.users.insertOne({
		"firstName": "John",
		"lastName": "Smith"
	});

	// INSERT MANY: To insert multiple documents, we can use insertMany()
	db.users.insertMany([
		{ "firstName": "John", "lastName": "Doe" },
		{ "firstName": "Jane", "lastName": "Doe" }
	]);


// R - READ

	// FIND: To get all the inserted users, we can use find()
	db.users.find();

	// FIND SPECIFIC: To find documents with a specified condition, an object should be passed to the find().
	db.users.find({ "lastName": "Doe" })
	// - This will find all users with Doe on their lastName


// U- UPDATE

	// UPDATE ONE:
	db.users.updateOne(
		{
			"_id": ObjectId("62b54b31b55e350b5950bb76")
		},
		{
			$set: {
				"email": "johnsmith@gmail.com"
			}
		}
	);
	// - The first argument contains the identifier on which documents to update and the second argument contains the properties to be updated
	// - Since email did not exist before the update, updateOne() will simply add the email property to the selected document. 

	// UPDATE MANY: 
	db.users.updateMany(
		{
			"lastName": "Doe"
		},
		{
			$set: {
				"isAdmin": false
			}
		}
	);
	// - Now, all documents with last name of "Doe" has a property of isAdmin with a value of false. 

// D - DELETE

// DELETE MANY: To update multiple documents, we can use deleteMany()
db.users.deleteMany({ "lastName": "Doe" });
// - Be careful when deleting data as you may delete data accidentally! 

// DELETE ONE: We can use deleteOne instead when we're sure we want to delete only one document.
db.users.deleteOne({ "_id": ObjectId("62b54b31b55e350b5950bb76") });


// How we operate on MongoDB and Robo3T 
// 1. Right click on the new connections. Then on the drop down choices, click "Create Database"
// 2. Decide on the name of the database
// 3. Right click on the new made database. Click on "Open Shell"
// 4. Type the code on the field where you are allowed to type the code
// 5. To execute the code, press F5 or Control + Enter

// What we did on MongoDB
// 1. We registered through gmail
// 2. We have chosen Amazon and Singapore respectively. After that, we have decided on the name of the cluster
// 3. We have added an IP on the Network Access. We have clicked "Allow access from anywhere"
// 4. We have set our username and password on the Quickstart


/*
	MongoDB CRUD Operation
	Create Retrieve Update Delete data from a NoSQL database.


	MongoDB Atlas is a MongoDB database located in the cloud and can be deployed in a cloud platform like Amazon Web Services, Azure, or Google Cloud Platform.

	MongoDB Community Server can be installed on your local machine. Howeverm since it is in your local machine, it is only accessible to the network you are currently in.

	Start by creating an account in MongoDB.

*/


/*
	CRUD Operations
	- Create, Read, Update, and Delete.

	Create:
		The create function allows ussers to create a new record in the detabase.
	Read:
		The read function is similar to search function. It allows user to search and retrieve specific records.
	Update:
		The update function is used to modify existing records that are on our database.
	Delete:
		The delete function allows users to remove records from a database that is no longer needed.
	

	Create: Insert Documents
	The mongo shell uses Javascript for its syntax.
	MongoDB deals with objects as it's structure for documents.
	We can create documents by providing objects into our methods.

	Javascript Syntax:
		object.object.method({object});

	
	INSERT ONE
	Syntax:
		db.collectionName.insertOne({object});

	Sample:
		db.users.insert({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact : {
				phone: "0987654321"
				email: "janedoe@gmail.com"
			}
		});


	INSERT MANY
	Syntax:
		db.collectionName.insertMany([
			{objectA},
			{objectB}
		]);

	Sample:
		db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact: {
					phone: "0987654321",
					email: "stephenhawking@gmail.com"
				},
				course: ["Python", "React", "PHP"],
				department: "none"
			},
			{
				firstName: "Neil",
				lastName: "Armstrong",
				age: 82,
				contact: {
					phone: "0987654321",
					email: "neilarmstrong@gmail.com"
				},
				course: ["React", "Laravel", "Sass"],
			},
		]);
*/

//READ: Find/Retrieve Documents
/*
	- The documents will be returned based on their order of storage in the collection.
*/

//FND All Documents
/*
	Syntax:
		db.collectionName.find();

	Sample:
	db.users.find();
*/

//FIND Using Single Parameter
/*
	Syntax:
		db.collectionName.find({ field: value });

	Sample:
	db.users.find({ firstName: "Stephen" });
*/

//FIND Using Multiple Parameter
/*
	Syntax:
		db.collectionName.find({ fieldA: valueA, fieldB: valueB })

	Sample:
	db.users.find({ lastName: "Armstrong", age: 82 })
*/

//FIND + Pretty Method
/*
	- The "pretty" method allows us to be able to view the documents returned by our terminal in a "prettier" format.

	Syntax:
		db.collectionName.finc({ field: value }).pretty();

	Sample:
	db.users.find({ lastName: "Armstrong", age: 82 }).pretty()
*/

//UPDATE: EDIT a document
/*
	Updating a Single Document

	Syntax:
		db.collectionName.updateOne( {criteria}, $set:{field: value});

	For our Example, let us create a document that we will then update...
	1. Insert initial document
		db.users.insert({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact: {
				phone: "00000",
				email: "test@gmail.com"
			},
			course: [],
			department: "none"
		});
	2. Update the document
		db.users.updateOne(
			{ firstName: "Test" },
			{
				$set: {
					firstName: "Bill",
					lastName: "Gates",
					age: 65,
					contact: {
					phone: "87654321",
					email: "bill@gmail.com"
					},
					course: ["PHP", "Laravel", "HTML"];
					department: "Operations",
					status: "active"
				}
			}
		);


*/

/*
	Mini Activity
	1. Change the contents of the document that contains "Test" as its first name using updateOne. Update the document with the below details.
	2. Return/view/read the document using users.find
	3. Use the pretty method
	4. Screenshot the returned document and paste it on our hangouts.

	firstName: "Bill",
	lastName: "Gates",
	age: 65,
	contact: {
		phone: "87654321",
		email: "bill@gmail.com"
	},
	course: ["PHP", "Laravel", "HTML"];
	department: "Operations",
	status: "active"
*/

//UPDATE MANY: Updating Multiple Documents
/*
	Syntax:
		db.collectionName.updateMany( {criteria}, {$set {field: value}});

	Sample:
	db.users.updateMany(
		{ department: "none" },
		{
			$set: { department: "HR" }
		}
	);
*/

//REPLACE ONE: Preplaces the whole document
/*
	- Replace one replaces the whole document.
	- if updateOne updates specific fields, replaceOne replaces the whole document.
	- If updateOne

	ang updateOne ay inuupdate ang isang field lang. ang replaceOne naman ay buong document.

db.users.replaceOne(
	{ firstName: "Bill" },
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "123456",
			email: "gates@hotmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
);
*/

//DELETE: Deleting Documents
/*
	for our example, let us create a document that we will delete

	it is a good practice to soft delete

db.users.insert({
	firstName: "Test"
});
*/

//DELETE ONE: Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria});

	Sample:
db.users.deleteOne({
	firstName: "Test"
});
db.users.find({ firstName: "Test" }).pretty();
*/

//DELETE MANY: Delete many documents
/*
	Syntax:
	db.collectionName.deleteMany({criteria});

	Sample:
db.users.deleteMany({
	firstName: "Bill"
});
db.users.find({ firstName: "Bill" }).pretty();
*/

//soft deletion, hindi natin dinilit kundi pinapalitan nalang natin yung mga values nya.


//DELETE ALL: Delete all documents
/*
	Syntax:
	db.users.deleteMany({});
*/

//ADVANCE QUERIES
/*
	- Retrieve data with complex data structures is also a good skill for any developer to have.
	- Real world examples of data can be as complex as having two or more layers of nested objects
	- learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application.
*/

//QUERY an embedded document
/*
	- An embedded document are those types of documents that contain a document inside a document.

	Sample:
	db.users.find({
		contact: {
			phone: "0987654321",
			email: "stephenhawking@gmail.com"
		}
	}).pretty();
*/

//QUERY on nested field
/*
	db.user.find({
		"contact.email": "janedoe@gmail.com"
	}).pretty();
*/

//QUERYING an array with exact elements
/*
	db.users.find({
		courses: ["CSS", "Javascript", "Python"]
	}).pretty();
*/

//QUERYING an array without regard to order
/*
	db.users.find({ courses: {$all: ["React", "Python"]} }).pretty();
*/

//QUERYING an embedded array
/*
	db.users.insertOne({
		nameArr: [
			{
				nameA: "Juan"
			},
			{
				nameB: "Tamad"
			}
		]
	});
db.users.find({
	nameArr:
	{
		nameA: "Juan"
	}
}).pretty();
*/